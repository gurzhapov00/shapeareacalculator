using NUnit.Framework;
using ShapeAreaCalculator.Models;

namespace UnitTests;

public class Tests
{
    [Test]
    public void CircleAreaCalculation()
    {
        IShape circle = new Circle(5);
        Assert.AreEqual(78.53981633974483, circle.CalculateArea(), 0.0001);
    }

    [Test]
    public void TriangleAreaCalculation()
    {
        IShape triangle = new Triangle(3, 4, 5);
        Assert.AreEqual(6, triangle.CalculateArea(), 0.0001);
    }

    [Test]
    public void RightAngledTriangleCheck()
    {
        var triangle = new Triangle(3, 4, 5);
        Assert.IsTrue(triangle.IsRightAngled());
    }
}