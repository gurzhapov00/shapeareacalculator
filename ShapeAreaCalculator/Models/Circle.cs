namespace ShapeAreaCalculator.Models;

/// <summary>
/// Круг.
/// </summary>
public class Circle : IShape
{
    /// <summary>
    /// Радиус.
    /// </summary>
    private readonly double _radius;

    /// <summary>
    /// Конструктор.
    /// </summary>
    /// <param name="radius">Радиус круга.</param>
    public Circle(double radius)
    {
        _radius = radius;
    }

    /// <summary>
    /// Площадь круга.
    /// </summary>
    /// <returns>Площадь круга.</returns>
    public double CalculateArea()
    {
        return Math.PI * _radius * _radius;
    }
}