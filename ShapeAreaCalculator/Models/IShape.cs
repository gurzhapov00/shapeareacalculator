namespace ShapeAreaCalculator.Models;

/// <summary>
/// Интерфейс для фигур, способных вычислить свою площадь.
/// </summary>
public interface IShape
{
    /// <summary>
    /// Вычисляет площадь фигуры.
    /// </summary>
    /// <returns>Площадь фигуры.</returns>
    double CalculateArea();
}