namespace ShapeAreaCalculator.Models;

/// <summary>
/// Треугольник.
/// </summary>
public class Triangle : IShape
{
    private readonly double _sideA;
    private readonly double _sideB;
    private readonly double _sideC;

    /// <summary>
    /// Конструктор.
    /// </summary>
    /// <param name="sideA">Длина стороны A.</param>
    /// <param name="sideB">Длина стороны B.</param>
    /// <param name="sideC">Длина стороны C.</param>
    public Triangle(double sideA, double sideB, double sideC)
    {
        _sideA = sideA;
        _sideB = sideB;
        _sideC = sideC;
    }

    /// <summary>
    /// Вычисляет площадь треугольника по формуле Герона.
    /// </summary>
    /// <returns>Площадь треугольника.</returns>
    public double CalculateArea()
    {
        var s = (_sideA + _sideB + _sideC) / 2;
        return Math.Sqrt(s * (s - _sideA) * (s - _sideB) * (s - _sideC));
    }

    /// <summary>
    /// Проверяет, является ли треугольник прямоугольным.
    /// </summary>
    /// <returns>True, если треугольник прямоугольный, иначе false.</returns>
    public bool IsRightAngled()
    {
        double[] sides = { _sideA, _sideB, _sideC };
        Array.Sort(sides);
        return Math.Pow(sides[0], 2) + Math.Pow(sides[1], 2) - Math.Pow(sides[2], 2) == 0;
    }
}